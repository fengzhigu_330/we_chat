package wx.bean;

import java.util.List;

public class ResultBean {
	private boolean isSucess;//标志请求是否成功
	private Object result;//返回的字符串(多数为json)
	private String resultMsg;
	public boolean isSucess() {
		return isSucess;
	}
	public void setSucess(boolean isSucess) {
		this.isSucess = isSucess;
	}
	public Object getResult() {
		return result;
	}
	public void setResult(Object result) {
		this.result = result;
	}
	public String getResultMsg() {
		return resultMsg;
	}
	public void setResultMsg(String resultMsg) {
		this.resultMsg = resultMsg;
	}
	
}
