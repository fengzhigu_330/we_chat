package wx.common.util.properties;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.Properties;

import org.apache.log4j.Logger;

public class PropertyUtil {
	static Logger log = Logger.getLogger(PropertyUtil.class);
	public static final String access_token_path;
	static{
		access_token_path = PropertyUtil.class.getClassLoader().getResource("access_token.properties").getPath();
		System.out.println("access_token_path=" + access_token_path);
	}
	/**
	 * 根据key获取properties文件的值
	 * @param key
	 */
	public static String getProperty(String key){
		Properties properties = new Properties();
		FileInputStream in = null;
		String value = null;
		try {
			in = new FileInputStream(access_token_path);
			properties.load(in);
			value = properties.getProperty(key);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			log.error("the file('" + access_token_path + "') not found! When getProperty by" + key);
			return null;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			log.error("IO error occur! When getProperty by" + key);
			return null;
		}finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					log.error("error while close FileInputStream!");
				}	
			}
		}
		return value;
	}
	
	public static void setProperty(String key, String value, boolean isAppend){
		Properties properties = new Properties();
		FileOutputStream out = null;
		try {
			out = new FileOutputStream(access_token_path, isAppend);
			properties.setProperty(key, value);
			properties.store(out, null);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			log.error("the file('" + access_token_path + "') not found! When setProperty by key=" + key + ",value=" + value);
			return;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			log.error("IO error occur!  When setProperty by key=" + key + ",value=" + value);
			return;
		}finally {
			if(out != null){
				try {
					out.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					log.error("error while close FileOutputStream!");
					return;
				}
			}
		}
		log.info("success set: key=" + key + ",value=" + value);
	}
	
}
