package wx.common.util.accessToken;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import net.sf.json.JSONObject;
import sun.nio.cs.ext.ISCII91;
import wx.common.util.http.HttpUtil;
import wx.common.util.properties.PropertyUtil;
import wx.constant.WXAccount;

public class AccessTokenUtil {
	Logger log = Logger.getLogger(AccessTokenUtil.class);
	
	public synchronized String getAccessToken(){
		Long time = System.currentTimeMillis();
		String expires_in = PropertyUtil.getProperty("expires_in");
		String access_token = PropertyUtil.getProperty("access_token");
		if (expires_in == null || access_token == null 
				|| expires_in.isEmpty() || access_token.isEmpty()) {
			Map<String, String> map = getAccessTokenOnline();
			boolean isAppend = true;
			for (String key : map.keySet()) {
				isAppend = !isAppend;
				if (key.equals("expires_in")) {
					time += Long.parseLong(map.get(key)) * 1000;
					PropertyUtil.setProperty(key, time.toString(), isAppend);
				}else if(key.equals("access_token")) {
					PropertyUtil.setProperty(key, map.get(key), isAppend);
				}
			}
			return map.get("access_token");
		}else {
			if (time > Long.parseLong(expires_in)) {
				log.info("access_token is invalid,begin updating!");
				Map<String, String> map = getAccessTokenOnline();
				boolean isAppend = true;
				for (String key : map.keySet()) {
					isAppend = !isAppend;
					if (key.equals("expires_in")) {
						time += Long.parseLong(map.get(key)) * 1000;
						PropertyUtil.setProperty(key, time.toString(), isAppend);
					}else if(key.equals("access_token")) {
						PropertyUtil.setProperty(key, map.get(key), isAppend);
					}
				}
				return map.get("access_token");
			}else {
				return access_token;
			}
		}
	}
	/**
	 * 获取接口调用凭证
	 * @param appid		
	 * @param appSecret
	 * @return
	 */
	public Map<String, String> getAccessTokenOnline(){
		Map<String, String> result = new HashMap<String, String>();
		String url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=" + WXAccount.appid + "&secret=" + WXAccount.appSecret;
		String response = "";
		try {
			response = HttpUtil.get(url);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if (response != "") {
			JSONObject object = JSONObject.fromObject(response);
			if (object.containsKey("expires_in") && object.containsKey("access_token")) {
				result.put("expires_in", object.getString("expires_in"));
				result.put("access_token", object.getString("access_token"));
				log.info("Get access_token:" + result.toString());
			}
		}
		return result;
	}
	
	public static void main(String[] args) {
		new AccessTokenUtil().getAccessToken();
	}
}
