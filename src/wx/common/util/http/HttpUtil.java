package wx.common.util.http;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.Proxy.Type;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.ServletContext;

import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.multipart.MultipartRequestEntity;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.FormBodyPart;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.ContentBody;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;

import com.oracle.jrockit.jfr.ContentType;

public class HttpUtil{
	private static String charset = "utf-8";
	
	
	public static HttpResponse get1(String url) throws Exception{
		HttpClient client = HttpClientBuilder.create().build();
		HttpGet get = new HttpGet(url);
		get.setHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
		get.setHeader("Accept-Encoding", "gzip, deflate, sdch");
		get.setHeader("Accept-Language", "zh-CN,zh;q=0.8");
		get.setHeader("Cache-Control", "no-cache");
		get.setHeader("Connection", "keep-alive");
		get.setHeader("Pragma", "no-cache");
		get.setHeader("Upgrade-Insecure-Requests", "1");
		get.setHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.104 Safari/537.36 Core/1.53.2372.400 QQBrowser/9.5.10548.400");
		HttpResponse response = null;
		response = client.execute(get);
		return response;
	}
	/**
	 * 发送 post请求，返回响应结果
	 * @param url		发送的目的服务器地址
	 * @param params	post的参数
	 * @return
	 * @throws Exception
	 */	
	public static String post(String url, Map<String, Object> params) throws Exception{
		HttpClient client = HttpClientBuilder.create().build();
		URL url1 = new URL(url);   
        URI uri = new URI(url1.getProtocol(), url1.getHost(), url1.getPath(), url1.getQuery(), null);
		HttpPost post = new HttpPost(uri);
		MultipartEntityBuilder builder = MultipartEntityBuilder.create();
		for (String key : params.keySet()) {
			Object value = params.get(key);
			if (value instanceof File) {
				builder.addBinaryBody(key, (File)params.get(key));
			}else if(value instanceof String) {
				builder.addTextBody(key, params.get(key).toString());
			}
		}
		HttpEntity entity1 = builder.build();
		post.setEntity(entity1);
		
		HttpResponse response = null;
		response = client.execute(post);
		String responseText = "";
		if(response.getStatusLine().getStatusCode() == HttpStatus.SC_OK){
			HttpEntity entity = response.getEntity();
			responseText = EntityUtils.toString(entity);
		}
		
		return responseText;
	}
	/**
	 * 发送post
	 * @param url
	 * @param jsonstring	json字符串参数
	 * @return				字符串
	 * @throws Exception
	 */
	public static String post(String url, String jsonstring) throws Exception{
		HttpClient client = HttpClientBuilder.create().build();
		URL url1 = new URL(url);   
        URI uri = new URI(url1.getProtocol(), url1.getHost(), url1.getPath(), url1.getQuery(), null);
        HttpPost post = new HttpPost(uri);
		StringEntity s = new StringEntity(jsonstring, charset);
		s.setContentEncoding("utf-8");
		s.setContentType("application/json");
		post.setEntity(s);
		HttpResponse response = client.execute(post);
		String responseText = "";
		if(response.getStatusLine().getStatusCode() == HttpStatus.SC_OK){
	        HttpEntity entity = response.getEntity();
	        InputStream in = entity.getContent();
	        int len;
	        byte[] buffer = new byte[1024];
	        ByteArrayOutputStream out = new ByteArrayOutputStream();
	        while((len = in.read(buffer)) != -1){
	        	out.write(buffer);
	        }
	        out.close();
	        in.close();
	        responseText = new String(out.toByteArray(), charset);
	    }
		return responseText;
	}
	
	/**
	 * 如果得到的是图片，将图片保存到本地指定目录
	 * @param url
	 * @return
	 * @throws Exception
	 */
	public static String get(String url) throws Exception{
		HttpClient client = HttpClientBuilder.create().build();
		URL url1 = new URL(url);   
        URI uri = new URI(url1.getProtocol(), url1.getHost(), url1.getPath(), url1.getQuery(), null);
		HttpGet post = new HttpGet(uri);
		
		HttpResponse response = null;
		response = client.execute(post);
		String responseText = "";
		if(response != null){
			String contentType = response.getFirstHeader("Content-Type").getValue();
			HttpEntity entity = response.getEntity();
			InputStream in = entity.getContent();
			if (!contentType.equals("image/jpeg")) {
				ByteArrayOutputStream out = new ByteArrayOutputStream();
				byte[] buffer = new byte[1024];
				int len = -1;
				while((len = in.read(buffer)) != -1){
					out.write(buffer, 0, len);
				}
				out.close();
				in.close();
				byte[] result = out.toByteArray();
				responseText = new String(result, charset);
			}else {
				String absoluteUrl = System.getProperty("user.dir");
				FileOutputStream out = new FileOutputStream(new File(absoluteUrl + "/WebContent/img/test.jpg"));
				byte[] temp = new byte[1024];
				int len = -1;
				while ((len = in.read(temp)) != -1) {
					out.write(temp, 0, len);
				}
				temp = null;
				out.close();
				in.close();
			}
		}
		return responseText;
	}
}
