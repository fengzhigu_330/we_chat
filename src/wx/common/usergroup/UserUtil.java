package wx.common.usergroup;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import wx.common.usergroup.bean.User;
import wx.common.usergroup.bean.UserList;
import wx.common.usergroup.interfaces.UserInterface;
import wx.common.util.accessToken.AccessTokenUtil;
import wx.common.util.http.HttpUtil;

public class UserUtil implements UserInterface{

	@Override
	public User getUserDetail(User user) {
		// TODO Auto-generated method stub
		String accessToken = new AccessTokenUtil().getAccessToken();
		String url = String.format(getUserDetailUrl, accessToken, user.getOpenid());
		String resultString = null;
		try {
			resultString = HttpUtil.get(url);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		User resultUser = null;
		if (resultString != null) {
			JSONObject object = JSONObject.fromObject(resultString);
			if (object.containsKey("errcode")) {
				resultUser = null;
			}else {
				resultUser = (User)JSONObject.toBean(object, User.class);
			}
		}
		
		return resultUser;
	}

	@Override
	public boolean remark(User user) {
		// TODO Auto-generated method stub
		String accessToken = new AccessTokenUtil().getAccessToken();
		String url = String.format(remarkUserUrl, accessToken);
		String paramString = "{\"openid\":\"" + user.getOpenid() 
			+ "\",\"remark\":\"" + user.getRemark() + "\"}";
		String resultString = null;
		try {
			resultString = HttpUtil.post(url, paramString);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		boolean isSuccess = false;
		if (null != resultString) {
			JSONObject object = JSONObject.fromObject(resultString);
			if (object.containsKey("errcode") && object.getInt("errcode") == 0) {
				isSuccess = true;
			}
		}
		
		return isSuccess;
	}

	@Override
	public UserList getMultDetail(List<User> list) {
		// TODO Auto-generated method stub
		String accessToken = new AccessTokenUtil().getAccessToken();
		String url = String.format(getMultDetailUrl, accessToken);
		String paramString = "";
		for (int i = 0; i < list.size(); i++) {
			paramString += "{\"openid\": \"" + list.get(i).getOpenid() 
					+ "\", \"lang\": \"zh_CN\"},";
		}
		paramString = paramString.substring(0, paramString.length() - 1);
		paramString = "[" + paramString + "]";
		paramString = "{\"user_list\":" + paramString + "}";
		String resultString = null;
		try {
			resultString = HttpUtil.post(url, paramString);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		UserList userList= null;
		if (resultString != null) {
			JSONObject object = JSONObject.fromObject(resultString);
			Map<String, Class> map = new HashMap<String, Class>();
			map.put("user_info_list", User.class);
			userList = (UserList)JSONObject.toBean(object, UserList.class, map);
		}
		return userList;
	}

	@Override
	public Map<String, Object> getSubScribe(String next_openid) {
		// TODO Auto-generated method stub
		String accessToken = new AccessTokenUtil().getAccessToken();
		String url = "";
		if (next_openid != null) {
			url = String.format(getSubscribeUrl, accessToken, next_openid);
		}else {
			url = String.format(getOriginSubscribeUrl, accessToken);
		}
		String resultString = null;
		try {
			resultString = HttpUtil.get(url);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Map<String, Object> map = new HashMap<String, Object>();
		if (resultString != null) {
			JSONObject object = JSONObject.fromObject(resultString);
			if (!object.containsKey("errcode")) {
				map.put("total", object.getInt("total"));
				map.put("count", object.getInt("count"));
				map.put("next_openid", object.getString("next_openid"));
				JSONObject dataObject = JSONObject.fromObject(object.get("data"));
				JSONArray openidArray = JSONArray.fromObject(dataObject.get("openid"));
				map.put("openid", openidArray);
			}
		}
		return map;
	}
	public static void main(String[] args) {
/*		User user = new User();
		user.setOpenid("oTZqOt1YbJ7a5OKDW2L5plc9Lep8");
		new UserUtil().getUserDetail(user);*/
		/*User user = new User();
		user.setOpenid("oTZqOt1YbJ7a5OKDW2L5plc9Lep8");
		user.setRemark("����");
		new UserUtil().remark(user);*/
/*		List<User> list = new ArrayList<User>();
		User user = new User();
		user.setOpenid("oTZqOt-5Z9qk4vWiECMcVbE1Hedw");
		User user1 = new User();
		user1.setOpenid("oTZqOt1YbJ7a5OKDW2L5plc9Lep8");
		list.add(user1);
		list.add(user);
		new UserUtil().getMultDetail(list);*/
		new UserUtil().getSubScribe("oTZqOt1YbJ7a5OKDW2L5plc9Lep8");
	}




}
