package wx.common.usergroup.interfaces;

import java.util.List;

import wx.common.usergroup.bean.User;
import wx.common.usergroup.bean.UserGroup;

public interface UserGroupInterface {
	public final String createUrl = "https://api.weixin.qq.com/cgi-bin/groups/create?access_token=%s";
	public final String getAllUrl = "https://api.weixin.qq.com/cgi-bin/groups/get?access_token=%s";
	public final String getGroupOfUserUrl = "https://api.weixin.qq.com/cgi-bin/groups/getid?access_token=%s";
	public final String modifyNameUrl = "https://api.weixin.qq.com/cgi-bin/groups/update?access_token=%s";
	public final String moveOneUserUrl = "https://api.weixin.qq.com/cgi-bin/groups/members/update?access_token=%s";
	public final String moveMulUserUrl = "https://api.weixin.qq.com/cgi-bin/groups/members/batchupdate?access_token=%s";
	public final String delGroupUrl = "https://api.weixin.qq.com/cgi-bin/groups/delete?access_token=%s";

	//新增用于分组
	String createUserGroup(UserGroup userGroup);
	//查询所有分组
	List<UserGroup> getAll();
	//查询用户所在分组
	String locationUserGroup(User user);
	//修改分组名
	boolean modifyGroupName(UserGroup group);
	//移动用户分组
	boolean moveOneUser(User user);
	//批量移动用户分组
	boolean movemulUser(List<User> userList);
	//删除分组
	boolean delUserGroup(UserGroup group);
}
