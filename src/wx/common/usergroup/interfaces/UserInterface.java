package wx.common.usergroup.interfaces;

import java.util.List;
import java.util.Map;

import wx.common.usergroup.bean.User;
import wx.common.usergroup.bean.UserList;

public interface UserInterface {
	public final String getUserDetailUrl = "https://api.weixin.qq.com/cgi-bin/user/info?access_token=%s&openid=%s&lang=zh_CN";
	public final String getMultDetailUrl = "https://api.weixin.qq.com/cgi-bin/user/info/batchget?access_token=%s";
	public final String remarkUserUrl = "https://api.weixin.qq.com/cgi-bin/user/info/updateremark?access_token=%s";
	public final String getSubscribeUrl = "https://api.weixin.qq.com/cgi-bin/user/get?access_token=%s&next_openid=%s";
	public final String getOriginSubscribeUrl = "https://api.weixin.qq.com/cgi-bin/user/get?access_token=%s";
	//获取单个用户基本信息
	User getUserDetail(User user);
	//设置备注名
	boolean remark(User user);
	//批量获取用户基本信息
	UserList getMultDetail(List<User> list);
	//获取用户列表
	Map<String, Object> getSubScribe(String next_openid);
}
