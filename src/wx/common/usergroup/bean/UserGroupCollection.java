package wx.common.usergroup.bean;

import java.util.List;

public class UserGroupCollection {
	private List<UserGroup> groups;

	public List<UserGroup> getGroups() {
		return groups;
	}

	public void setGroups(List<UserGroup> groups) {
		this.groups = groups;
	}
	
}
