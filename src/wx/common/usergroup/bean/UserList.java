package wx.common.usergroup.bean;

import java.util.List;

/**
 * 批量用户列表
 * @author qiao
 *
 */
public class UserList {
	private List<User> user_info_list;

	public List<User> getUser_info_list() {
		return user_info_list;
	}

	public void setUser_info_list(List<User> user_info_list) {
		this.user_info_list = user_info_list;
	}
	
}	
