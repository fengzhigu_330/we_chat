package wx.common.usergroup;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONObject;
import wx.common.usergroup.bean.User;
import wx.common.usergroup.bean.UserGroup;
import wx.common.usergroup.bean.UserGroupCollection;
import wx.common.usergroup.interfaces.UserGroupInterface;
import wx.common.util.accessToken.AccessTokenUtil;
import wx.common.util.http.HttpUtil;

public class UserGroupUtil implements UserGroupInterface {
	
	@Override
	public String createUserGroup(UserGroup userGroup) {
		// TODO Auto-generated method stub
		String accessToken = new AccessTokenUtil().getAccessToken();
		String url = String.format(createUrl, accessToken);
		String paramString = "{\"group\":{\"name\":\"" + userGroup.getName() + "\"}}";
		String result = "invoke interface fail";
		try {
			result = HttpUtil.post(url, paramString);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public List<UserGroup> getAll() {
		// TODO Auto-generated method stub
		String accessToken = new AccessTokenUtil().getAccessToken();
		String url = String.format(getAllUrl, accessToken);
		String resultString = "";
		UserGroupCollection collection = null;
		List<UserGroup> list = null;
		try {
			resultString = HttpUtil.get(url);
			JSONObject object = JSONObject.fromObject(resultString);
			Map<String, Class> classMap = new HashMap<String, Class>();
			classMap.put("groups", UserGroup.class);
			collection = (UserGroupCollection)JSONObject.toBean(object, UserGroupCollection.class, classMap);
			if(collection != null){
				list = collection.getGroups();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return list;
	}	
	
	@Override
	public String locationUserGroup(User user) {
		// TODO Auto-generated method stub
		String accessToken = new AccessTokenUtil().getAccessToken();
		String url = String.format(getGroupOfUserUrl, accessToken);
		String paramString = "{\"openid\":\"" + user.getOpenid() + "\"}";
		String resultString = null;
		try {
			resultString = HttpUtil.post(url, paramString);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String groupid = null;
		if (resultString != null) {
			JSONObject object = JSONObject.fromObject(resultString);
			if(object.containsKey("errcode")){
				
			}else {
				groupid = object.getString("groupid");
			}
		}
		return groupid;
	}
	
	@Override
	public boolean modifyGroupName(UserGroup userGroup){
		String accessToken = new AccessTokenUtil().getAccessToken();
		String url = String.format(modifyNameUrl, accessToken);
		String paramString = "{\"group\":{\"id\":" + userGroup.getId() 
			+ ",\"name\":\"" + userGroup.getName() + "\"}}";
		boolean isSuccess = false;
		String resultString = null;
		try {
			resultString = HttpUtil.post(url, paramString);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(resultString != null){
			JSONObject object = JSONObject.fromObject(resultString);
			if (object.containsKey("errcode") && object.getString("errcode").equals("0")) {
				isSuccess = true;
			}
		}
		return isSuccess;
	}
	@Override
	public boolean moveOneUser(User user) {
		// TODO Auto-generated method stub
		String accessToken = new AccessTokenUtil().getAccessToken();
		String url = String.format(moveOneUserUrl, accessToken);
		String paramString = "{\"openid\":\"" + user.getOpenid() 
			+ "\",\"to_groupid\":" + user.getGroupid() + "}";
		String resultString = null;
		try {
			resultString = HttpUtil.post(url, paramString);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		boolean isSuccess = false;
		if (resultString != null) {
			JSONObject object = JSONObject.fromObject(resultString);
			if (object.containsKey("errcode") && object.getInt("errcode") == 0) {
				isSuccess = true;
			}
		}
		
		return isSuccess;
	}
	@Override
	public boolean movemulUser(List<User> userList){
		// TODO Auto-generated method stub
		String accessToken = new AccessTokenUtil().getAccessToken();
		String url = String.format(moveMulUserUrl, accessToken);
		String arrayOpenid = "";
		for (int i = 0; i < userList.size(); i++) {
			arrayOpenid += "\"" + userList.get(i).getOpenid() + "\",";
		}
		arrayOpenid = arrayOpenid.substring(0, arrayOpenid.length() - 1);
		arrayOpenid = "[" + arrayOpenid + "]";
		String paramString = "{\"openid_list\":" + arrayOpenid 
				+ ",\"to_groupid\":" + userList.get(0).getGroupid() + "}";
		String resultString = null;
		try {
			resultString = HttpUtil.post(url, paramString);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		boolean isSuccess = false;
		if(resultString != null){
			JSONObject object = JSONObject.fromObject(resultString);
			if (object.containsKey("errcode") && object.getInt("errcode") == 0) {
				isSuccess = true;
			}
		}
		return isSuccess;
	}
	@Override
	public boolean delUserGroup(UserGroup group) {
		// TODO Auto-generated method stub
		String accessToken = new AccessTokenUtil().getAccessToken();
		String url = String.format(delGroupUrl, accessToken);
		String paramString = "{\"group\":{\"id\":" + group.getId() + "}}";
		String resultString = null;
		try {
			resultString = HttpUtil.post(url, paramString);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		boolean isSuccess = false;
		if (resultString != null) {
			JSONObject object = JSONObject.fromObject(resultString);
			if (object.containsKey("errcode") && object.getInt("errcode") == 0) {
				isSuccess = true;
			}
		}
		return isSuccess;
	}
}
