package wx.common.mail;

import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;

public class MailTool {
	
	/**
	 *  发送到qq邮箱
	 * @param strMail
	 * @param strTitle	邮件标题
	 * @param strText	邮件内容
	 * @return
	 */
	private static boolean send_qqmail(String fromMail, String strMail, String strTitle, String strText){
        boolean bret = false;
        try
        {
            final Properties props = new Properties();

            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.host", "smtp.qq.com");
            //你自己的邮箱
            props.put("mail.user", fromMail); 
            //你开启pop3/smtp时的验证码
            props.put("mail.password", "ezhyvdldfogzbjjh");
            props.put("mail.smtp.port", "25");
            props.put("mail.smtp.starttls.enable", "true");

            Authenticator authenticator = new Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication() {
                    String userName = props.getProperty("mail.user");
                    String password = props.getProperty("mail.password");
                    return new PasswordAuthentication(userName, password);
                }
            };
            // 使用环境属性和授权信息，创建邮件会话
            Session mailSession = Session.getInstance(props, authenticator);
            // 创建邮件消息
            MimeMessage message = new MimeMessage(mailSession);
            // 设置发件人
            String username = props.getProperty("mail.user");
            InternetAddress form = new InternetAddress(username);
            message.setFrom(form);

            InternetAddress to = new InternetAddress(strMail); 
            message.setRecipient(RecipientType.TO, to);

            // 设置邮件标题
            message.setSubject(strTitle);

            // 设置邮件的内容体
            message.setContent(strText, "text/html;charset=UTF-8");

            // 发送邮件
            Transport.send(message);
            bret = true;
        }
        catch (AddressException e) {
             e.printStackTrace();
        }
        catch (MessagingException e) {
             e.printStackTrace();
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return bret;
    }
	
	/**
	 * 发送到163邮箱
	 * @param strMail
	 * @param strTitle	邮件标题
	 * @param strText	邮件内容
	 * @return
	 */
	private static boolean send_163mail(String fromMail, String fromMailPWD, String strMail, String strTitle, String strText){
        boolean bret = false;
        try
        {
            final Properties props = new Properties();
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.host", "smtp.163.com");

            // 发件人的账号
            props.put("mail.user", fromMail);
            //发件人的密码
            props.put("mail.password", fromMailPWD); 

            // 构建授权信息，用于进行SMTP进行身份验证
            Authenticator authenticator = new Authenticator() {
                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    // 用户名、密码
                    String userName = props.getProperty("mail.user");
                    String password = props.getProperty("mail.password");
                    return new PasswordAuthentication(userName, password);
                }
            };
            // 使用环境属性和授权信息，创建邮件会话
            Session mailSession = Session.getInstance(props, authenticator);
            // 创建邮件消息
            MimeMessage message = new MimeMessage(mailSession);
            // 设置发件人
            String username = props.getProperty("mail.user");
            InternetAddress form = new InternetAddress(username);
            message.setFrom(form);

            // 设置收件人
            InternetAddress to = new InternetAddress(strMail); 
            message.setRecipient(RecipientType.TO, to);

            // 设置邮件标题
            message.setSubject(strTitle);

            // 设置邮件的内容体
            message.setContent(strText, "text/html;charset=UTF-8");
            // 发送邮件
            Transport.send(message);
            bret = true;
        }
        catch (AddressException e) {
             e.printStackTrace();
        }
        catch (MessagingException e) {
             e.printStackTrace();
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return bret;
    }
	
	public static void main(String[] args) {
		send_qqmail("275908541@qq.com", "2194702688@qq.com", "Testing", "hello, it's a test!!");
	}
}
