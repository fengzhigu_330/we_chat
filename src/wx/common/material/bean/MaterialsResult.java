package wx.common.material.bean;

import java.util.List;

/**
 * 获取素材列表得到的结果bean
 * @author qiao
 *
 */
public class MaterialsResult<T extends MaterialOther> {
	private Integer total_count;//	该类型的素材的总数
	private Integer item_count;//	本次调用获取的素材的数量
	private List<T> item;
	public Integer getTotal_count() {
		return total_count;
	}
	public void setTotal_count(Integer total_count) {
		this.total_count = total_count;
	}
	public Integer getItem_count() {
		return item_count;
	}
	public void setItem_count(Integer item_count) {
		this.item_count = item_count;
	}
	public List<T> getItem() {
		return item;
	}
	public void setItem(List<T> item) {
		this.item = item;
	}
	
	
}
