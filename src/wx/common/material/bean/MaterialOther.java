package wx.common.material.bean;

public class MaterialOther extends Material{
	private String name;//   文件名称
	private String url;//	图文页的URL，或者，当获取的列表是图片素材列表时，该字段是图片的URL
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	
}
