package wx.common.material.bean;

import java.util.List;

public class Content {
	private List<NewsItem> news_item;

	public List<NewsItem> getNews_item() {
		return news_item;
	}

	public void setNews_item(List<NewsItem> news_item) {
		this.news_item = news_item;
	}
	
}
