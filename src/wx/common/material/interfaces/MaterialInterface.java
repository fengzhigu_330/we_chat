package wx.common.material.interfaces;

public class MaterialInterface {
	//上传临时素材
	public static final String upload_temp_material_url = "https://api.weixin.qq.com/cgi-bin/media/upload?access_token=%s&type=%s";
	//获取临时素材
	public static final String get_temp_material_url = "https://api.weixin.qq.com/cgi-bin/media/get?access_token=%s&media_id=%s";
	
	//上传图文消息内的图片获取URL
	public static final String upload_persistent_news_img_url = "https://api.weixin.qq.com/cgi-bin/media/uploadimg?access_token=%s";
	//上传永久素材(图文素材)
	public static final String upload_persistent_news_url = "https://api.weixin.qq.com/cgi-bin/material/add_news?access_token=%s";
	//上传永久素材
	public static final String upload_persistent_material_url = "https://api.weixin.qq.com/cgi-bin/material/add_material?access_token=%s"; 
	//获取永久素材
	public static final String get_persistent_material_url = "https://api.weixin.qq.com/cgi-bin/material/get_material?access_token=%s";
	
}
