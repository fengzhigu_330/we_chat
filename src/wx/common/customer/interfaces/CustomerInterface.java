package wx.common.customer.interfaces;

import java.util.List;

import wx.common.customer.bean.Customer;

/**
 * 
 * @author qiao
 * 客服接口
 *
 */
public interface CustomerInterface {
	String CREAT_URL = "https://api.weixin.qq.com/customservice/kfaccount/add?access_token=%s";
	String UPDATE_URL = "https://api.weixin.qq.com/customservice/kfaccount/update?access_token=%s";
	String DELETE_URL = "https://api.weixin.qq.com/customservice/kfaccount/del?access_token=%s";
	String SET_HEAD_IMG_URL = "http://api.weixin.qq.com/customservice/kfaccount/uploadheadimg?access_token=%s&kf_account=%s";
	String GET_ALL_URL = "https://api.weixin.qq.com/cgi-bin/customservice/getkflist?access_token=%s";	
	
	String CREATE_PARAMS = "{\"kf_account\" : \"%s\",\"nickname\" : \"%s\",\"password\" : \"%s\",}";
	String UPDATE_PARAMS = "{\"kf_account\" : \"%s\",\"nickname\" : \"%s\",\"password\" : \"%s\",}";
	String DELETE_PARAMS = "{\"kf_account\" : \"test1@test\",\"nickname\" : \"客服1\",\"password\" : \"pswmd5\",}";
	
	//添加客服账号
	public boolean createCustomer(Customer customer);
	
	//修改客服账号
	public boolean updateCustomer(Customer customer);
	
	//删除客服账号
	public boolean deleteCutomer(Customer customer);
	
	//设置客服帐号的头像
	public boolean setHeadImg(Customer customer);
	
	//获取所有客服账号
	public List<Customer> getAllCutomers();
}
