package wx.common.customer;

import java.util.List;

import net.sf.json.JSONObject;
import wx.common.customer.bean.Customer;
import wx.common.customer.interfaces.CustomerInterface;
import wx.common.util.accessToken.AccessTokenUtil;
import wx.common.util.http.HttpUtil;

public class CustomerUtil extends AccessTokenUtil implements CustomerInterface{
//gh_53de325d820e
	@Override
	public boolean createCustomer(Customer customer) {
		// TODO Auto-generated method stub
		String access_token = getAccessToken();
		String url = String.format(CREAT_URL, access_token);
		String json_params = String.format(CREATE_PARAMS, customer.getKf_account(),
				customer.getNickname(), customer.getPassword());
		String result = null;
		try {
			result = HttpUtil.post(url, json_params);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (result != null) {
			JSONObject object = JSONObject.fromObject(result);
			if (object.getString("errmsg") != null && object.getString("errmsg").equals("ok")) {
				return true;
			}
		}
		System.out.println(result);
		return false;
	}

	@Override
	public boolean updateCustomer(Customer customer) {
		// TODO Auto-generated method stub
		String access_token = getAccessToken();
		String url = String.format(UPDATE_URL, access_token);
		String json_params = String.format(UPDATE_PARAMS, customer.getKf_account(),
				customer.getNickname(), customer.getPassword());
		return false;
	}

	@Override
	public boolean deleteCutomer(Customer customer) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean setHeadImg(Customer customer) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<Customer> getAllCutomers() {
		// TODO Auto-generated method stub
		String access_token = getAccessToken();
		String url = String.format(GET_ALL_URL, access_token);
		String result = null;
		try {
			result = HttpUtil.get(url);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (null != result) {
			JSONObject object = JSONObject.fromObject(result);
			if (object.containsKey("kf_list")) {
				Object kf_list = object.getJSONObject("kf_list");
				System.out.println("-----------");
			}
		}
		return null;
	}
	
}
