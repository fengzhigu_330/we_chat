package wx.common.menu;

import java.net.URL;
import java.net.URLEncoder;

import org.apache.log4j.Logger;

import net.sf.json.JSONObject;
import wx.constant.WXAccount;
import wx.common.util.accessToken.AccessTokenUtil;
import wx.common.util.http.HttpUtil;

public class MenuUtil extends AccessTokenUtil{
	Logger log = Logger.getLogger(MenuUtil.class);
	
	/**
	 * 删除公众号当前所有菜单
	 */
	public void delAllMenu(){
		String access_token = getAccessToken();
		String url = "https://api.weixin.qq.com/cgi-bin/menu/delete?access_token=" + access_token;
		String response = "";
		try {
			response = HttpUtil.get(url);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (response.equals("{\"errcode\":0,\"errmsg\":\"ok\"}")) {
			log.info("删除菜单成功！");
		}
	}
	
	/**
	 * 创建菜单
	 * @return
	 */
	public boolean createMenuButtons(String buttons){
		String access_token = getAccessToken();
		String api_url = " https://api.weixin.qq.com/cgi-bin/menu/create?access_token=" + access_token;
		buttons = "{\"button\":[{\"type\":\"click\",\"name\":\"I LOVE U\",\"key\":\"_love\"},"
				+ "{\"type\":\"view\",\"name\":\"进入百度\",\"url\":\"https://www.baidu.com\"}]}";
		String response = "";
		try {
			response = HttpUtil.post(api_url, buttons);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		JSONObject object = JSONObject.fromObject(response);
		if (object.getString("errmsg").equals("ok")) {
			return true;
		}else{
			return false;
		}
	}
}
