package wx.common.menu;

public class MenuParam {
	//点击推事件
	public static final String clickType = "{\"type\":\"click\",\"name\":\"%s\",\"key\":\"%s\"}";
	
	//跳转URL
	public static final String viewType = "{\"type\":\"view\",\"name\":\"%s\",\"url\":\"%s\"}";
	
	//扫码推事件
	public static final String scancode_pushType = "{\"type\": \"scancode_waitmsg\",\"name\": \"%s\", \"key\": \"%s\", \"sub_button\": [ ]}";
	
	//扫码推事件且弹出“消息接收中”提示框
	public static final String scancode_waitmsgType = "{\"type\": \"scancode_waitmsg\",\"name\": \"%s\", \"key\": \"%s\", \"sub_button\": [ ]}";

	//弹出系统拍照发图
	public static final String pic_sysphotoType = "{\"type\": \"pic_sysphoto\",\"name\": \"%s\",\"key\": \"%s\",\"sub_button\": [ ]}";

	//弹出拍照或者相册发图
	public static final String pic_photo_or_albumType = " {\"type\": \"pic_photo_or_album\",\"name\": \"%s\",\"key\": \"%s\",\"sub_button\": [ ]}";

	//弹出微信相册发图器
	public static final String pic_weixinType = "{\"type\": \"pic_weixin\",\"name\": \"%s\",\"key\": \"%s\",\"sub_button\": [ ]}";

	//弹出地理位置选择器
	public static final String location_selectType = "{\"name\": \"%s\",\"type\": \"location_select\",\"key\": \"%s\"}";
	
	//下发消息（除文本消息）
	public static final String media_idType = "{\"type\": \"media_id\",\"name\": \"%s\",\"media_id\": \"%s\"}";

	//跳转图文消息URL
	public static final String view_limitedType = "{\"type\": \"view_limited\",\"name\": \"%s\",\"media_id\": \"%s\"}";
}
