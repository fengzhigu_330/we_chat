package wx.common.msg;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import wx.bean.MSG;
import wx.common.News;
import wx.common.msg.bean.ReplyMsg;
import wx.common.msg.interfaces.ReplyInterface;

public class ReplyUtil implements ReplyInterface{
	
	//回复文本消息
	@Override
	public void replyText(MSG msg, HttpServletResponse response, String message){
		String replyXml = String.format(ReplyMsg.textMsgXml, msg.getFromUserName(), msg.getToUserName(), 
				System.currentTimeMillis()/1000, message);
		print(response, replyXml);
	}
	
	//回复图片消息
	@Override
	public void replyImg(MSG msg, HttpServletResponse response, String media_id){
		String replyXml = String.format(ReplyMsg.imgMsgXml, msg.getFromUserName(), msg.getToUserName(), 
				System.currentTimeMillis()/1000, media_id);
		print(response, replyXml);
	}
	
	//回复语音消息
	@Override
	public void replyVoice(MSG msg, HttpServletResponse response, String media_id){
		String replyXml = String.format(ReplyMsg.voiceMsgXml, msg.getFromUserName(), msg.getToUserName(), 
				System.currentTimeMillis()/1000, media_id);
		print(response, replyXml);
	}
	
	//回复视频消息
	@Override
	public void replyVedio(MSG msg, HttpServletResponse response, String media_id, String title, String description){
		String replyXml = String.format(ReplyMsg.videoMsgXml, msg.getFromUserName(), msg.getToUserName(), 
				System.currentTimeMillis()/1000, media_id, title, description);
		print(response, replyXml);
	}
	
	//回复音乐消息
	@Override
	public void replyMusic(MSG msg, HttpServletResponse response, String title, String description, String music_url, 
			String hqMusicUrl, String media_id){
		String replyXml = String.format(ReplyMsg.musicMsgXml, msg.getFromUserName(), msg.getToUserName(), 
				System.currentTimeMillis()/1000, title, description, music_url, hqMusicUrl, media_id);
		print(response, replyXml);
	}
	
	//回复图文消息
	@Override
	public void replyNews(MSG msg, HttpServletResponse response, List<News> newsList){
		List<Object> list = new ArrayList<Object>();
		list.add(msg.getFromUserName());
		list.add(msg.getToUserName());
		list.add(System.currentTimeMillis()/1000);
		list.add(newsList.size());
		for (int i = 0; i < newsList.size(); i++) {
			list.add(newsList.get(i).getTitle());
			list.add(newsList.get(i).getDescription());
			list.add(newsList.get(i).getPicUrl());
			list.add(newsList.get(i).getUrl());
		}
		String replyXml = String.format(ReplyMsg.getNewsMsgXml(newsList.size()), list);
		print(response, replyXml);
	}
	
	public void print(HttpServletResponse response, String replyXml){
		PrintWriter writer = null;
		try {
			response.setCharacterEncoding("utf-8");
			response.setContentType("text/plain");
			writer = response.getWriter();
			writer.print(replyXml);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			writer.close();
		}
	}
}
