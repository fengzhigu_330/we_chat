package wx.common.msg;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import wx.bean.MSG;
import wx.common.usergroup.UserUtil;
import wx.common.usergroup.bean.User;

public class ReceiveUtil {
	static Logger log = null;
	static ReplyUtil reply = null;
	static {
		reply = new ReplyUtil();
		log = Logger.getLogger(ReceiveUtil.class);
	}
	/**
	 * 接受微信服务器发送的xml数据，并响应 
	 * @param msg
	 */
	public static void recieveMsg(MSG msg, HttpServletResponse response){
		String type = msg.getMsgType();
		String event = msg.getEvent();
		if(type.equals("text")) {//接收文本消息
			
		}else if(type.equals("image")){//接收图片消息
			reply.replyText(msg, response, "收到图片！");
		}else if(type.equals("voice")){//接收语音消息
			
		}else if(type.equals("video")){//接收
			
		}else if(type.equals("shortvideo")){
			
		}else if(type.equals("location")){
			
		}else if(type.equals("link")){
			
		}else if (type.equals("event")) {
			log.info("receive a event from wexin platform......");
			if (event.equals("subscribe")) {//关注时推送的事件
				log.info("receive a event of subscribe......");
				String eventKey = msg.getEventKey();
				String ticket = msg.getTicket();
				if(eventKey != null && ticket != null){//扫码关注
					reply.replyText(msg, response, "欢迎扫描二维码关注本公众号！EventKey=" 
							+ eventKey + ",Ticket=" + ticket);
				}else {
					User user = new User();
					user.setOpenid(msg.getFromUserName());
					user = new UserUtil().getUserDetail(user);
					reply.replyText(msg, response, "欢迎[" + user.getNickname() + "]关注本公众号！");
				}
			}else if (event.equals("unsubscribe")) {//取消关注时推送的事件
				reply.replyText(msg, response, "你取消了关注本公众号！");
			}else if (event.equals("SCAN")) {//用户已关注时候的扫码事件
				String eventKey = msg.getEventKey();
				String ticket = msg.getTicket();
				reply.replyText(msg, response, "你已关注本公众号！EventKey=" 
						+ eventKey + ",Ticket=" + ticket);
			}else if(event.equals("LOCATION")){//上报地理位置
				String latitude = msg.getLatitude();//纬度
				String longitude = msg.getLongitude();//经度
				String precision = msg.getPrecision();
				reply.replyText(msg, response, "收到你的地理位置信息如下：latitude="+latitude
						+",longitude="+longitude+",precision="+precision);
			}else if (event.equals("CLICK")) {//点击自定义菜单的事件
				String eventKey = msg.getEventKey();
				reply.replyText(msg, response, "你点击了自定义菜单。自定义菜单的key=" + eventKey);
			}else if (event.equals("VIEW")) {//点击自定义菜单跳转链接的事件
				String eventKey = msg.getEventKey();
				reply.replyText(msg, response, "你点击了自定义菜单。即将跳转的url=" + eventKey);
			}
		}
	}
}
