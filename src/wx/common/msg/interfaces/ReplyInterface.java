package wx.common.msg.interfaces;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import wx.bean.MSG;
import wx.common.News;

public interface ReplyInterface {
	//回复文本消息
	void replyText(MSG msg, HttpServletResponse response, String message);
	//回复图片消息
	void replyImg(MSG msg, HttpServletResponse response, String media_id);
	//回复语音消息
	void replyVoice(MSG msg, HttpServletResponse response, String media_id);
	//回复视频消息
	void replyVedio(MSG msg, HttpServletResponse response, String media_id, String title, String description);
	//回复音乐消息
	void replyMusic(MSG msg, HttpServletResponse response, String title, String description, String music_url, 
			String hqMusicUrl, String media_id);
	//回复图文消息
	void replyNews(MSG msg, HttpServletResponse response, List<News> newsList);
}
