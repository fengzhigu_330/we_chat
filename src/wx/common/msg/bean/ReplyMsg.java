package wx.common.msg.bean;

/**
 * 被动回复用户消息 模板
 * @author qiao
 *
 */
public class ReplyMsg {
	//文本消息
	public static final String textMsgXml = "<xml>"
			+ "<ToUserName><![CDATA[%s]]></ToUserName>"
			+ "<FromUserName><![CDATA[%s]]></FromUserName>"
			+ "<CreateTime>%d</CreateTime>"
			+ "<MsgType><![CDATA[text]]></MsgType>"
			+ "<Content><![CDATA[%s]]></Content>"
			+ "</xml>";
	//图片消息
	public static final String imgMsgXml = "<xml>"
			+ "<ToUserName><![CDATA[%s]]></ToUserName>"
			+ "<FromUserName><![CDATA[%s]]></FromUserName>"
			+ "<CreateTime>%d</CreateTime>"
			+ "<MsgType><![CDATA[image]]></MsgType>"
			+ "<Image>"
			+ "<MediaId><![CDATA[%s]]></MediaId>"
			+ "</Image>"
			+ "</xml>";
	
	//语音消息
	public static final String voiceMsgXml = "<xml>"
			+ "<ToUserName><![CDATA[%s]]></ToUserName>"
			+ "<FromUserName><![CDATA[%s]]></FromUserName>"
			+ "<CreateTime>%d</CreateTime>"
			+ "<MsgType><![CDATA[voice]]></MsgType>"
			+ "<Voice>"
			+ "<MediaId><![CDATA[%s]]></MediaId>"
			+ "</Voice>"	
			+ "</xml>";
	
	//视频消息
	public static final String videoMsgXml = "<xml>"
			+ "<ToUserName><![CDATA[%s]]></ToUserName>"
			+ "<FromUserName><![CDATA[%s]]></FromUserName>"
			+ "<CreateTime>%d</CreateTime>"
			+ "<MsgType><![CDATA[video]]></MsgType>"
			+ "<Video>"
			+ "<MediaId><![CDATA[%s]]></MediaId>"
			+ "<Title><![CDATA[%s]]></Title>"
			+ "<Description><![CDATA[%s]]></Description>"
			+ "</Video> "
			+ "</xml>";
	
	//音乐消息
	public static final String musicMsgXml = "<xml>"
			+ "<ToUserName><![CDATA[%s]]></ToUserName>"
			+ "<FromUserName><![CDATA[%s]]></FromUserName>"
			+ "<CreateTime>%d</CreateTime>"
			+ "<MsgType><![CDATA[music]]></MsgType>"
			+ "<Music>"
			+ "<Title><![CDATA[%s]]></Title>"
			+ "<Description><![CDATA[%s]]></Description>"
			+ "<MusicUrl><![CDATA[%s]]></MusicUrl>"
			+ "<HQMusicUrl><![CDATA[%s]]></HQMusicUrl>"
			+ "<ThumbMediaId><![CDATA[%s]]></ThumbMediaId>"
			+ "</Music>"
			+ "</xml>";
	
	//图文消息	
	public static final String getNewsMsgXml(int number){
		String xml = "<xml>"
				+ "<ToUserName><![CDATA[%s]]></ToUserName>"
				+ "<FromUserName><![CDATA[%s]]></FromUserName>"
				+ "<CreateTime>%d</CreateTime>"
				+ "<MsgType><![CDATA[news]]></MsgType>"
				+ "<ArticleCount>%d</ArticleCount>"
				+ "<Articles>";
		String articles = "";
		for (int i = 0; i < number; i++) {
			articles += "<item>"
					+ "<Title><![CDATA[%s]]></Title>"
					+ "<Description><![CDATA[%s]]></Description>"
					+ "<PicUrl><![CDATA[%s]]></PicUrl>"
					+ "<Url><![CDATA[url]]></Url>"
					+ "</item>";
		}
		xml += articles + "</Articles></xml>";
		return xml;
	}
	
}
