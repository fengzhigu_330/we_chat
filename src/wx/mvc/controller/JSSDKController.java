package wx.mvc.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import net.sf.json.JSONObject;
import wx.common.util.accessToken.AccessTokenUtil;
import wx.common.util.http.HttpUtil;
import wx.constant.WXAccount;

@Controller
@RequestMapping(value = "/JSSDK")
public class JSSDKController extends BaseController{

	@RequestMapping(value = "/test.do")
	public String test(){
		return "/admin/JSSDK/test.jsp";
	}
	@RequestMapping(value = "/ticket.do")
	public void getSignature(HttpServletRequest request, HttpServletResponse response, Model model){
		String weburl = request.getParameter("url");
		Long timestamp = System.currentTimeMillis()/1000;
		int noncestr = new Random().nextInt();
		
		AccessTokenUtil accessTokenUtil = new AccessTokenUtil();
		String accessToken = accessTokenUtil.getAccessToken();System.out.println("accessToken:" + accessToken);
		//��ȡjsapi_ticket
		String jsapi_ticket = null;
		try {
			String url = "https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token=" + accessToken + "&type=jsapi";
			String responseText = HttpUtil.get(url);
			jsapi_ticket = null;
			JSONObject object = JSONObject.fromObject(responseText);
			if (object.containsKey("ticket")) {
				jsapi_ticket = object.getString("ticket");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//����signature
		List<String> nameList = new ArrayList<String>();
		nameList.add("noncestr");
		nameList.add("timestamp");
		nameList.add("url");
		nameList.add("jsapi_ticket");
		Map<String, Object> valueMap = new HashMap<String, Object>();
		valueMap.put("noncestr", noncestr);
		valueMap.put("timestamp", timestamp);
		valueMap.put("url", weburl);
		valueMap.put("jsapi_ticket", jsapi_ticket);
		Collections.sort(nameList);
		String origin = "";
		for (int i = 0; i < nameList.size(); i++) {
			origin += nameList.get(i) + "=" + valueMap.get(nameList.get(i)).toString() + "&";
		}
		origin = origin.substring(0, origin.length() - 1);
		String signature = sha1(origin);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("jsapi_ticket", jsapi_ticket);
		map.put("appId", WXAccount.appid);
		map.put("signature", signature.toLowerCase());
		map.put("timestamp", timestamp.toString());
		map.put("noncestr", String.valueOf(noncestr));
		response.setContentType("application/json; charset=utf-8");
		PrintWriter writer = null;
		try {
			writer = response.getWriter();
//			JSONObject responseObject = JSONObject.fromObject(map);
//			writer.print(responseObject);
			outResponse(response, map);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			writer.flush();
			writer.close();
		}
		
	}
}
