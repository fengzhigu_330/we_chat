package wx.mvc.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;

import net.sf.json.JSONObject;
import wx.common.msg.ReceiveUtil;

public class BaseController{
	
	
	/**
	 * 排序并加密
	 * @param signature
	 * @param timestamp
	 * @param nonce
	 * @return
	 */
	public String orderAndSha1(String... params){
		Arrays.sort(params);
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < params.length; i++) {
			sb.append(params[i]);
		}
		MessageDigest md = null;
        String tmpStr = null;
		try {
			md = MessageDigest.getInstance("SHA-1");
			byte[] b = md.digest(sb.toString().getBytes());
			tmpStr = byteToStr(b);
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        sb = null;
		return tmpStr;
	}
	
	public String sha1(String origin){
		MessageDigest md = null;
        String tmpStr = null;
		try {
			md = MessageDigest.getInstance("SHA-1");
			byte[] b = md.digest(origin.getBytes());
			tmpStr = byteToStr(b);
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return tmpStr;
	}
	
	 /** 
     * 将字节数组转换为十六进制字符串 
     *  
     * @param byteArray 
     * @return 
     */  
    private static String byteToStr(byte[] byteArray) {  
        String strDigest = "";  
        for (int i = 0; i < byteArray.length; i++) {  
            strDigest += byteToHexStr(byteArray[i]);  
        }  
        return strDigest;  
    }  
  
    /** 
     * 将字节转换为十六进制字符串 
     *  
     * @param mByte 
     * @return 
     */  
    private static String byteToHexStr(byte mByte) {  
        char[] Digit = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };  
        char[] tempArr = new char[2];  
        tempArr[0] = Digit[(mByte >>> 4) & 0X0F];  
        tempArr[1] = Digit[mByte & 0X0F];  
  
        String s = new String(tempArr);  
        return s;  
    }  
    
    public void outResponse(HttpServletResponse response, Object object){
    	response.setContentType("application/json");
    	response.setCharacterEncoding("utf-8");
    	PrintWriter writer = null;
		try {
			writer = response.getWriter();
			JSONObject  jsonObject = JSONObject.fromObject(object);
			String res = jsonObject.toString();
			writer.write(res);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			if(writer != null)
			writer.close();
		}
    }
    
    public void outResponse(HttpServletResponse response, String text){
    	response.setContentType("text/plain");
    	response.setCharacterEncoding("utf-8");
    	PrintWriter writer = null;
		try {
			writer = response.getWriter();
			writer.write(text);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			if(writer != null)
			writer.close();
		}
    }
}
