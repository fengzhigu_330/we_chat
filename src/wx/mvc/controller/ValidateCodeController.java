package wx.mvc.controller;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.security.SecureRandom;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ValidateCodeController extends BaseController{

	/**
	 * 生成验证码
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/login_code.do")
	public void fetchCode(HttpServletRequest request, HttpServletResponse response){
		int width = 44;
		int height = 20;
		BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		Graphics g = image.getGraphics();
		
		SecureRandom random = new SecureRandom();
		//生成数字加字母的随机字符串
		String validation = "";
		for (int i = 0; i < 4; i++) {
			int charNum = random.nextInt(74) + 48;
			if ((charNum > 57 && charNum < 65) || (charNum > 90 && charNum < 97)) {
				i--;
				continue;
			}
			char randomCode = (char)charNum;
			validation += randomCode;
			g.drawString(String.valueOf(randomCode), i*11+1, 18);
		}
		System.out.println(validation);
		HttpSession session = request.getSession();
		session.setAttribute("login_code", validation);
		try {
			response.setContentType("image/jpeg");
			response.setHeader("Pragma","No-cache");
			response.setHeader("Cache-Control","no-cache");
			response.setDateHeader("Expires", 0);
			ImageIO.write(image, "jpeg", response.getOutputStream());
			g.dispose();
			image.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * 登录页面
	 * @return
	 */
	@RequestMapping(value = "/login_page.do")
	public String login(HttpServletRequest request, HttpServletResponse response){
		return "/admin/login/login_page.jsp";
	}
	
	@RequestMapping(value = "/login_check.do")
	public void check_login(HttpServletRequest request, HttpServletResponse response){
		String code = request.getParameter("login_code");
		response.setCharacterEncoding("utf-8");
		response.setContentType("plain/text");
		
		String result = "";
		if (code.equalsIgnoreCase(request.getSession().getAttribute("login_code").toString())) {
			request.getSession().setAttribute("is_login", "yes");
			result = "OK";
		}else {
			result = "ERR CODE";
		}
		outResponse(response, result);
	}
	
	@RequestMapping(value = "/logout.do")
	public String logout(HttpServletRequest request, HttpServletResponse response){
		request.getSession().invalidate();
		return "/admin/login/login_page.jsp";
	}
	
	public static void main(String[] args) {
		new ValidateCodeController().fetchCode(null, null);
	}
}
