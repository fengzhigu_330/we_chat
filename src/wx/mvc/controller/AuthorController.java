package wx.mvc.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import net.sf.json.JSON;
import net.sf.json.JSONObject;
import wx.common.util.http.HttpUtil;
import wx.constant.WXAccount;

/**
 * 授权
 * @author qiao
 *
 */
@Controller
@RequestMapping(value = "/auth")
public class AuthorController {
	
	@RequestMapping(value = "/fetch.do")
	public void fetchUserInfo(HttpServletRequest request, HttpServletResponse response){
		String code = request.getParameter("code");
		String state = request.getParameter("state");
		
		if (null != code) {//获取access_token
			Map<String, String> access_token_list = getAccess_token(code);
			getUserInfo(access_token_list);
		}
	}
	
	//通过code换取网页授权access_token
	public Map<String, String> getAccess_token(String code){
		Map<String, String> result = new HashMap<String, String>(); 
		String url = "https://api.weixin.qq.com/sns/oauth2/access_token?"
				+ "appid=%s&secret=%s&code=%s&grant_type=authorization_code";
		url = String.format(url, WXAccount.appid, WXAccount.appSecret, code);
		String response = null;
		try {
			response = HttpUtil.get(url);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (response != null) {
			JSONObject object = JSONObject.fromObject(response);
			if (object.containsKey("access_token")) {
				result.put("access_token", object.getString("access_token"));
			}
			if (object.containsKey("expires_in")) {
				result.put("expires_in", object.getString("expires_in"));
			}
			if (object.containsKey("refresh_token")) {
				result.put("refresh_token", object.getString("refresh_token"));
			}
			if (object.containsKey("expires_in")) {
				result.put("openid", object.getString("openid"));
			}
			if (object.containsKey("scope")) {
				result.put("scope", object.getString("scope"));
			}
		}
		
		return result;
	}
	
	
	//拉取用户信息
	public void getUserInfo(Map<String, String> access_token_list){
		//拉取用户信息
		String fetch_url = "https://api.weixin.qq.com/sns/userinfo?access_token=%s&openid=%s&lang=zh_CN";
		fetch_url = String.format(fetch_url, access_token_list.get("access_token"), access_token_list.get("openid"));
		String response = null;
		try {
			response = HttpUtil.get(fetch_url);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if (null != response) {
			JSONObject object = JSONObject.fromObject(response);
			if (!object.containsKey("errmsg")) {
				System.out.println("成功获取用户信息如下：\n" + object.toString());
			}
		}
	}
}
