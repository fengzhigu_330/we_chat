package wx.mvc.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import wx.bean.MSG;
import wx.common.msg.ReceiveUtil;
import wx.common.util.http.XMLUtil;
import wx.constant.WXAccount;

@Controller
@RequestMapping(value = "/index")
public class IndexController extends BaseController{

	/**
	 * 验证消息是否来自微信服务器，并且返回相应信息
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping(value = "/msg.do")
	public void msg(HttpServletRequest request, HttpServletResponse response) throws Exception{
		ServletInputStream sis = request.getInputStream();
		StringBuilder sb = new StringBuilder();
		int len;
		byte[] buffer = new byte[1024];
		while ((len = sis.read(buffer)) != -1) {
			sb.append(new String(buffer, 0, len, "utf-8"));
		}
		String xml = sb.toString();
		if (xml.length() == 0) {//微信服务器如果没有发送xml形式的参数，则最有可能是在验证url
			check(request, response);
			return;
		}
		
		//将xml转换为对象
		Object object = XMLUtil.XML2Entity(xml, MSG.class);
		MSG msg = object == null ? null : (MSG)object;
		
		ReceiveUtil.recieveMsg(msg, response);
	}
	
	/**
	 * 验证消息是否来自微信服务器
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/msg1.do")
	public void check(HttpServletRequest request, HttpServletResponse response) {
		String signature = request.getParameter("signature");
		String timestamp = request.getParameter("timestamp");
		String nonce = request.getParameter("nonce");
		String echostr = request.getParameter("echostr");
		
		String [] arr = new String[]{WXAccount.token, timestamp, nonce};
		String encode = orderAndSha1(arr);
		if (encode != null && encode.equalsIgnoreCase(signature)) {//sha1加密结构部分大小写
			PrintWriter writer = null;
			try {
				writer = response.getWriter();
				writer.print(echostr);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				if (writer != null) {
					writer.close();
				}
			}
		}
	}
}
