package wx.mvc.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.HttpResponse;
import org.omg.CosNaming.NamingContextExtPackage.StringNameHelper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.util.HtmlUtils;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import wx.bean.Article;
import wx.bean.NewsBean;
import wx.bean.ResultBean;
import wx.common.material.bean.Material;
import wx.common.material.bean.MaterialNews;
import wx.common.material.bean.MaterialOther;
import wx.common.material.bean.MaterialsResult;
import wx.common.material.interfaces.MaterialInterface;
import wx.common.util.accessToken.AccessTokenUtil;
import wx.common.util.http.HttpUtil;
import wx.common.util.properties.PropertyUtil;
@Controller
@RequestMapping(value = "/material")
public class MaterialController extends BaseController{
	
	/**
	 * 进入图片页面
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/imagepage.do")
	public String imagePage(HttpServletRequest request, HttpServletResponse response){
		return "/admin/image/index.jsp";
	}
	//上传临时素材
	@RequestMapping(value = "/uploadTemp.do")
	public static void upload_temp_material(HttpServletRequest request, HttpServletResponse response){
		String type = "image";
		String access_token = new AccessTokenUtil().getAccessToken();
		String url = String.format(MaterialInterface.upload_temp_material_url, access_token, type);
		String imgUrl = MaterialController.class.getClassLoader().getResource("two_dimension.jpg").getPath();
		Map<String, Object> params = new HashMap<String, Object>();
		File file = new File(imgUrl);
		params.put("media", file);
		String responseText;
		PrintWriter writer = null;
		try {
			responseText = HttpUtil.post(url, params);
			response.setContentType("text/plain");
			writer = response.getWriter();
			writer.println(responseText);
		} catch (Exception e) {
			// TODO: handle exception
		}finally {
			if (null != writer){	
				writer.flush();
				writer.close();
			}
		}
	}
	
	//获取临时素材
	@RequestMapping(value = "/getTemp.do")
	public static void get_temp_material(HttpServletRequest request, HttpServletResponse response){
		String media_id = request.getParameter("media_id");
		String access_token = new AccessTokenUtil().getAccessToken();
		String get_url = String.format(MaterialInterface.get_temp_material_url, access_token, media_id);
		String responseText = null;
		try {
			responseText = HttpUtil.get(get_url);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (responseText != null) {
			System.out.println(responseText);
		}
	}
	
	
	public static void upload_persistent_news_img(HttpServletRequest request, HttpServletResponse response){
		String accessToken = new AccessTokenUtil().getAccessToken();
		String url_upload = String.format(MaterialInterface.upload_persistent_news_img_url, accessToken);
		File file = new File(MaterialController.class.getClassLoader().getResource("two_dimension.jpg").getPath());
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("media", file);
		try {
			String responseText = HttpUtil.post(url_upload, params);
			System.out.println(responseText);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	/** 
	 * 新增永久素材
	 * 图文素材
	 */
	@RequestMapping(value = "/uploadPersistent.do")
	public static void upload_persistent_news(HttpServletRequest request, HttpServletResponse response){
		List<Article> articles = new ArrayList<Article>();
		Article article = new Article();
		article.setTitle("测试上传");
		article.setThumb_media_id("qsMKvEkcZKfj9lPLuJNhGPhEDaz25GVTwR0kgYg1wLc");
		article.setShow_cover_pic(1);
//		article.setDigest("摘要");
		article.setContent_source_url("https://www.baidu.com");
		article.setContent("我看到一个东西哈哈！");
		article.setAuthor("damo");
		articles.add(article);
		List<Article> list = new ArrayList<Article>();
		list.add(article);
		
		String accessToken = new AccessTokenUtil().getAccessToken();
		String url_upload = String.format(MaterialInterface.upload_persistent_news_url, accessToken);
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("articles", list);
		
		String responsetext = null;
		try {
			responsetext = HttpUtil.post(url_upload, JSONObject.fromObject(params).toString());
			System.out.println(responsetext);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * 新增永久素材
	 * 其他类型
	 * @param request
	 * @param response
	 */
	public static void upload_persistent_material(HttpServletRequest request, HttpServletResponse response){
		ResultBean result = new ResultBean();
		result.setSucess(false);
		File file = new File("");
		String access_token = new AccessTokenUtil().getAccessToken();
		String type/* = request.getParameter("type")*/;
		String url_upload = String.format(MaterialInterface.upload_persistent_material_url, access_token);
		Map<String, Object> params = new HashMap<String, Object>();
		type = "thumb";
		file = new File(MaterialController.class.getClassLoader().getResource("flower.jpg").getPath());
		params.put("type", type);
		params.put("media", file);
		
		PrintWriter write = null;
		Map<String, String> resMap = new HashMap<String, String>();
		try {
			String responseText = HttpUtil.post(url_upload, params);
			if (!responseText.isEmpty()) {
				JSONObject temp = JSONObject.fromObject(responseText);
				if (!temp.containsKey("errcode")) {//上传成功
					result.setSucess(true);
					result.setResult(responseText);
				}else {
					result.setSucess(false);
					result.setResult(responseText);
				}
				write = response.getWriter();
				write.print(result);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally{
			if (null != write) {
				write.close();
			}
		}
	}

	/**
	 * 获取永久素材列表
	 */
	@RequestMapping(value = "/persistentList.do")
	public void get_persistent_list(HttpServletRequest request, HttpServletResponse response){
		ResultBean result = new ResultBean();
		result.setSucess(false);
		String start = "0";
		String count = "100";
		List<Material> list = new ArrayList<Material>();
		while(true){
			String accessToken = new AccessTokenUtil().getAccessToken();
			String url = "https://api.weixin.qq.com/cgi-bin/material/batchget_material?access_token=" + accessToken;
			Map<String, String> params = new HashMap<String, String>();
			params.put("type", "image");
			params.put("offset", start);
			params.put("count", count);
			JSONObject obj = JSONObject.fromObject(params);
			MaterialsResult<MaterialOther> m = null;
			try {
				String responseText = HttpUtil.post(url, obj.toString());
				JSONObject o = JSONObject.fromObject(responseText);
				Map<String, Class> classMap = new HashMap<String, Class>();
				classMap.put("item", MaterialOther.class);
				m = (MaterialsResult<MaterialOther>)JSONObject.toBean(o, 
						MaterialsResult.class, classMap);
				list.addAll(m.getItem());
				Integer startindex = Integer.valueOf(start) + Integer.valueOf(count);
				start = String.valueOf(startindex);
				if (m.getItem_count() < 20) {//已经获取了所有
					break;
				}
			
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				result.setResultMsg("获取图片失败");
				outResponse(response, result);
				return;
			}
		}
		result.setSucess(true);
		result.setResultMsg("获取图片成功！");
		result.setResult(list);
		outResponse(response, result);
	}
	//测试通过返回流的方式，在页面显示图片
	public void testStream(HttpServletRequest request, HttpServletResponse response){
		String imgUrl = MaterialController.class.getClassLoader().getResource("two_dimension.jpg").getPath();
		FileInputStream stream = null;
		PrintWriter writer = null;
		try {
			stream = new FileInputStream(new File(imgUrl));
			response.setContentType("image/jpeg");
			writer = response.getWriter();
			int length = stream.read();
			while (length != -1) {
				writer.write((char)length);
				length = stream.read();
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			try {
				stream.close();
				writer.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * 代理输出图片
	 * @param args
	 */
	@RequestMapping(value = "/policyimg.do")
	public void policyImg(HttpServletRequest request, HttpServletResponse response){
		Map<String, Object> params = new HashMap<String, Object>();
		String url = request.getParameter("url");
		HttpResponse httpResponse = null;
		PrintWriter writer = null;
		InputStream in = null;
		
		try {
			httpResponse = HttpUtil.get1(url);
			in = httpResponse.getEntity().getContent();
			writer = response.getWriter();
			int len = -1;
			len = in.read();
			while(len != -1){
				writer.write((char)len);
//				writer.write(HtmlUtils.htmlEscape(String.valueOf((char)len)));
				len = in.read();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			if (writer != null) {
				writer.close();
			}
			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

}
