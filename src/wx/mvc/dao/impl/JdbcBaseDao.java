package wx.mvc.dao.impl;

import java.util.List;

import org.springframework.jdbc.core.support.JdbcDaoSupport;

public class JdbcBaseDao extends JdbcDaoSupport {
	
	public List<Object> query(String sql){
		return this.getJdbcTemplate().queryForList(sql, Object.class);
	}
}
