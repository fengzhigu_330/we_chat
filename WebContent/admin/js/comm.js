function initialPage(params){
	var headerParam = params.header;
	page_header(headerParam);
	page_img(params.imgParams);
}

//组合header的html代码
function page_header(params){
	var origin_header = "<h1>{parent}" +
			"<small><i class=\"icon-double-angle-right\">" +
			"</i>{son}</small></h1>";
	var normal = origin_header.format(params);
	$("#page-header").empty().append(normal);
}

function page_img(params){
	var origin_a_html = '<a href="{ahref}" title="Photo Title" data-rel="colorbox">{img}</a>';
	var origin_img_html = '<img height="150" width="150" alt="150x150" src="{imgsrc}" />';
	var lis = "";
	for(var i=0; i<params.length; i++){
		var a_html = origin_a_html.format(params[i]);
		var img_html = origin_img_html.format(params[i]);
		a_html = a_html.format({img:img_html});
		lis += '<li>' + a_html + '</li>';
	}
	$("#thumbimg ul").empty().append(lis);
}

String.prototype.format = function(args) {
    var result = this;
    if (arguments.length > 0) {    
        if (arguments.length == 1 && typeof (args) == "object") {
            for (var key in args) {
                if(args[key]!=undefined){
                    var reg = new RegExp("({" + key + "})", "g");
                    result = result.replace(reg, args[key]);
                }
            }
        }
        else {
            for (var i = 0; i < arguments.length; i++) {
                if (arguments[i] != undefined) {
                	var reg= new RegExp("({)" + i + "(})", "g");
                    result = result.replace(reg, arguments[i]);
                }
            }
        }
    }
    return result;
}
