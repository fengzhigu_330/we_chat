<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<html>
<head>
<c:set var="basePath" value="<%=basePath%>" scope="request"/>
<base href="<%=basePath%>">

<meta name="viewport" content="width=device-width,initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="description" content="overview &amp; stats" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Insert title here</title>

<%@include file="../../admin/common/common.jsp" %>
</head>

<body>

<!-- 页头 -->
<%@include file="../../admin/common/nav.jsp" %>

<div class="main-container" id="main-container">
	<script type="text/javascript">
        try {
            ace.settings.check('main-container', 'fixed')
        } catch(e) {}
    </script>
	
	<div class="main-container-inner">
		<a class="menu-toggler" id="menu-toggler" href="#">
			<span class="menu-text"></span>
		</a>
		
		<%@include file="../../admin/common/siderBar.jsp" %>
		
		<div class="main-content">
			<div class="breadcrumbs" id="breadcrumbs">
                <script type="text/javascript">
                    try {
                        ace.settings.check('breadcrumbs', 'fixed')
                    } catch(e) {}
                </script>
                <ul class="breadcrumb">
                    <li>
                        <i class="icon-home home-icon">
                        </i>
                        <a href="#">
                            Home
                        </a>
                    </li>
                    <li class="active">
                        Dashboard
                    </li>
                </ul>
            </div>
			
			<div id="nav_header" class="page-content">
                
            </div>
            
            <div class="page-content">
            	<div class="col-xs-12">
            		
            		<form class="form-horizontal" role="form">
						<div class="form-group">
							<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> APPID </label>
	
							<div class="col-sm-9">
								<input type="text" id="appid" placeholder="APPID" class="col-xs-10 col-sm-5" />
							</div>
						</div>
	
						<div class="space-4"></div>
	
						<div class="form-group">
							<label class="col-sm-3 control-label no-padding-right" for="form-field-2"> APPSECRET </label>
	
							<div class="col-sm-9">
								<input type="password" id="appsecret" placeholder="Password" class="col-xs-10 col-sm-5" />
							</div>
						</div>
	
						<div class="space-4"></div>
	
						<div class="form-group">
							<label class="col-sm-3 control-label no-padding-right" for="form-field-4">TOKEN</label>
	
							<div class="col-sm-9">
								<input class="col-xs-10 col-sm-5" type="text" id="token" placeholder="TOKEN" />
							</div>
						</div>
	
						<div class="clearfix form-actions">
							<div class="col-md-offset-3 col-md-9">
								<button class="btn btn-info" type="button">
									<i class="icon-ok bigger-110"></i>
									确定
								</button>
	
								&nbsp; &nbsp; &nbsp;
								<button class="btn" type="reset">
									<i class="icon-undo bigger-110"></i>
									取消
								</button>
							</div>
						</div>
					</form>
            	</div>
            </div>
            
		</div>
	</div>
</div>

<h1>图片如下：</h1>
<img src="${basePath }material/policyimg.do?url=http://mmbiz.qpic.cn/mmbiz_jpg/kw0SNp762WnH7BzyULc6ZQPLUS8LTphc8g2NrbusDwYNjOuUb4KWMia5OwMvdiaU4SgAI6DP0R67Jqea8ER4auqQ/0?wx_fmt=jpeg"/>

		<script type="text/javascript">
			window.jQuery || document.write("<script src='${basePath }assets/js/jquery-2.0.3.min.js'>"+"<"+"/script>");
		</script>

		<!-- <![endif]-->

		<!--[if IE]>
<script type="text/javascript">
 window.jQuery || document.write("<script src='assets/js/jquery-1.10.2.min.js'>"+"<"+"/script>");
</script>
<![endif]-->

		<script type="text/javascript">
			if("ontouchend" in document) document.write("<script src='${basePath }assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
		</script>
		<script src="${basePath }assets/js/bootstrap.min.js"></script>
		<script src="${basePath }assets/js/typeahead-bs2.min.js"></script>

		<!-- page specific plugin scripts -->

		<!--[if lte IE 8]>
		  <script src="assets/js/excanvas.min.js"></script>
		<![endif]-->

		<script src="${basePath }assets/js/jquery-ui-1.10.3.custom.min.js"></script>
		<script src="${basePath }assets/js/jquery.ui.touch-punch.min.js"></script>
		<script src="${basePath }assets/js/jquery.slimscroll.min.js"></script>
		<script src="${basePath }assets/js/jquery.easy-pie-chart.min.js"></script>
		<script src="${basePath }assets/js/jquery.sparkline.min.js"></script>

		<!-- ace scripts -->

		<script src="${basePath }assets/js/ace-elements.min.js"></script>
		<script src="${basePath }assets/js/ace.min.js"></script>

		<!-- inline scripts related to this page -->

		<script type="text/javascript" src = "${basePath }admin/js/comm.js"></script>
		<script type="text/javascript">
		$(function(){
			var params = {};
			var headerParam = {parent:"微信", son:"配置&应用"};
			params.header = headerParam;
			
			initialPage(params);
		});
		</script>
</body>
</html>