<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%@ page isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fn" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html> 
<head>
<c:set var="basePath" value="<%=basePath%>" scope="request"/>
<base href="<%=basePath%>">

<meta name="viewport" content="width=device-width,initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="description" content="overview &amp; stats" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Insert title here</title>

<%@include file="../../admin/common/common.jsp" %>
<%@include file="../base/taglibs.jsp" %>
</head>

<body>
<!-- 页头 -->
<%@include file="../../admin/common/nav.jsp" %>

<div class="main-container" id="main-container">
	<script type="text/javascript">
        try {
            ace.settings.check('main-container', 'fixed')
        } catch(e) {}
    </script>
	
	<div class="main-container-inner">
		<a class="menu-toggler" id="menu-toggler" href="#">
			<span class="menu-text"></span>
		</a>
		
		<%@include file="../../admin/common/siderBar.jsp" %>
		
		<div class="main-content">
			<div class="breadcrumbs" id="breadcrumbs">
                <script type="text/javascript">
                    try {
                        ace.settings.check('breadcrumbs', 'fixed')
                    } catch(e) {}
                </script>
                <ul class="breadcrumb">
                    <li>
                        <i class="icon-home home-icon">
                        </i>
                        <a href="#">
                            Home
                        </a>
                    </li>
                    <li class="active">
                        Images
                    </li>
                </ul>
            </div>
			
			<div id="nav_header" class="page-content">
                <div class="page-header" id="page-header">
            	</div>
            
	            <div class="row" id="thumbimg">
					<div class="col-xs-12">
	            		<div class="row-fluid">
	            			<ul class="ace-thumbnails">
	            			
	            			</ul>
	            		</div>
	            	</div>
	            </div>
            </div>
		</div>
	</div>
</div>



		<script type="text/javascript">
			window.jQuery || document.write("<script src='${basePath }assets/js/jquery-2.0.3.min.js'>"+"<"+"/script>");
		</script>

		<!-- <![endif]-->

		<!--[if IE]>
<script type="text/javascript">
 window.jQuery || document.write("<script src='assets/js/jquery-1.10.2.min.js'>"+"<"+"/script>");
</script>
<![endif]-->

		<script type="text/javascript">
			if("ontouchend" in document) document.write("<script src='${basePath }assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
		</script>
		<script src="${basePath }assets/js/bootstrap.min.js"></script>
		<script src="${basePath }assets/js/typeahead-bs2.min.js"></script>

		<!-- page specific plugin scripts -->

		<!--[if lte IE 8]>
		  <script src="assets/js/excanvas.min.js"></script>
		<![endif]-->

		<script src="${basePath }assets/js/jquery-ui-1.10.3.custom.min.js"></script>
		<script src="${basePath }assets/js/jquery.ui.touch-punch.min.js"></script>
		<script src="${basePath }assets/js/jquery.slimscroll.min.js"></script>
		<script src="${basePath }assets/js/jquery.easy-pie-chart.min.js"></script>
		<script src="${basePath }assets/js/jquery.sparkline.min.js"></script>
		<script src="${basePath }assets/js/jquery.colorbox-min.js"></script>
		<!-- ace scripts -->

		<script src="${basePath }assets/js/ace-elements.min.js"></script>
		<script src="${basePath }assets/js/ace.min.js"></script>
		<script src="${basePath }assets/js/jquery.gritter.min.js"></script>

		<!-- inline scripts related to this page -->

		<script type="text/javascript" src = "${basePath }admin/js/comm.js"></script>
		<script type="text/javascript">
		$(function(){
			var params = {};
			var headerParam = {parent:"素材", son:"管理"};
			params.header = headerParam;
			
			var imgArray = new Array();
			//动态从微信服务器获取永久素材图片
			$.ajax({
				url: '${basePath}material/persistentList.do',
				dataType: 'json',
				async: false,
				success: function(res){
					if(res.sucess){
						var item = res.result;
						if(item && item.length > 0){
							for(var i=0; i<item.length; i++){
								var url = item[i].url;
								//代理图片
								url = '${basePath}material/policyimg.do?url='+url;
								imgArray.push({imgsrc:url,ahref:url+'.jpg'});//colorbox可能判断后缀确定，所以加上.jpg
							}
							params.imgParams = imgArray;
						}
						jQuery.gritter.add({
							//title: options.errorTitle,
							text: res.resultMsg ? res.resultMsg : '成功',
							//sticky: true,//不自动隐藏
							class_name: 'gritter-success  gritter-light'
						});
					}else{
						jQuery.gritter.add({
							//title: options.errorTitle,
							text: res.resultMsg ? res.resultMsg : '错误',
							//sticky: true,//不自动隐藏
							class_name: 'gritter-error  gritter-light'
						});
					}
					initialPage(params);
				},
				error:function(){
					
				},
				complete:function(){
					
				}
			});
			
			var colorbox_params = {
				reposition:true,
				scalePhotos:true,
				scrolling:false,
				previous:'<i class="icon-arrow-left"></i>',
				next:'<i class="icon-arrow-right"></i>',
				close:'&times;',
				current:'{current} of {total}',
				maxWidth:'100%',
				maxHeight:'100%',
				onOpen:function(){
					document.body.style.overflow = 'hidden';
				},
				onClosed:function(){
					document.body.style.overflow = 'auto';
				},
				onComplete:function(){
					$.colorbox.resize();
				}
			};

			$('.ace-thumbnails [data-rel="colorbox"]').colorbox(colorbox_params);
			$("#cboxLoadingGraphic").append("<i class='icon-spinner orange'></i>");
		});
		</script>
</body>
</html>