<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<head>
<c:set var="basePath" value="<%=basePath%>" scope="request"/>
<base href="<%=basePath%>">

<meta name="viewport" content="width=device-width,initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Insert title here</title>
<script type="text/javascript" src="${basePath }assets/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
<script type="text/javascript" src="${basePath }assets/encrypt/md5AndSha1.js"></script>

<script type="text/javascript">
$(function(){
	var url = window.location.href;
	//ajax注入权限验证
	$.ajax({
		url:"${basePath }JSSDK/ticket.do",
		dataType: 'json',
		data: {"url" : url},
		complete: function(XMLHttpRequest, textStatus){
			
		},
		error: function(XMLHttpRequest, textStatus, errorThrown){
			alert("发生错误："+errorThrown);
		},
		success: function(res){
			var appId = res.appId;
			var noncestr = res.noncestr;
			var jsapi_ticket = res.jsapi_ticket;
			var timestamp = res.timestamp;
			var signature = res.signature;
			wx.config({
			    debug: true, //开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
			    appId: appId, //必填，公众号的唯一标识
			    timestamp: timestamp, // 必填，生成签名的时间戳
			    nonceStr: noncestr, //必填，生成签名的随机串
			    signature: signature,// 必填，签名，见附录1
			    jsApiList: ['onMenuShareTimeline','onMenuShareAppMessage','onMenuShareQQ',
			                'onMenuShareWeibo','onMenuShareQZone','chooseImage',
			                'uploadImage','downloadImage','startRecord','stopRecord',
			                'onVoiceRecordEnd','playVoice','pauseVoice','stopVoice',
			                'translateVoice','openLocation','getLocation','hideOptionMenu',
			                'showOptionMenu','closeWindow','hideMenuItems','showMenuItems',
			                'showAllNonBaseMenuItem','hideAllNonBaseMenuItem'] //必填，需要使用的JS接口列表，所有JS接口列表 见附录2
			});
		}
	});

});

wx.error(function(res){
	alert("权限验证失败！:" + JSON.stringify(res));
    //config信息验证失败会执行error函数，如签名过期导致验证失败，具体错误信息可以打开config的debug模式查看，也可以在返回的res参数中查看，对于SPA可以在这里更新签名。
	
});

//config信息验证后会执行ready方法，所有接口调用都必须在config接口获得结果之后，config是一个客户端的异步操作，所以如果需要在页面加载时就调用相关接口，则须把相关接口放在ready函数中调用来确保正确执行。对于用户触发时才调用的接口，则可以直接调用，不需要放在ready函数中。
wx.ready(function(){
	//分享到朋友圈
	wx.onMenuShareTimeline({
	    title: '张苗', //分享标题
	    link: 'https://www.baidu.com', //分享链接
	    imgUrl: 'https://gss0.bdstatic.com/70cFsj3f_gcX8t7mm9GUKT-xh_/avatar/100/r6s1g4.gif', //分享图标
	    success: function () { 
	        //用户确认分享后执行的回调函数
	    },
	    cancel: function () { 
	        //用户取消分享后执行的回调函数
	    }
	});

	//分享给朋友
	wx.onMenuShareAppMessage({
	    title: '送你一只小狗', //分享标题
	    desc: '这是一个测试项目,纯属娱乐', //分享描述
	    link: 'http://image.baidu.com/search/detail?ct=503316480&z=0&ipn=d&word=%E7%8B%97%E7%8B%97%E5%9B%BE%E7%89%87&step_word=&hs=0&pn=9&spn=0&di=72743929170&pi=0&rn=1&tn=baiduimagedetail&is=0%2C0&istype=2&ie=utf-8&oe=utf-8&in=&cl=2&lm=-1&st=-1&cs=3916307322%2C789181421&os=1743761458%2C1908584243&simid=0%2C0&adpicid=0&lpn=0&ln=1977&fr=&fmq=1486188250728_R&fm=index&ic=0&s=undefined&se=&sme=&tab=0&width=&height=&face=undefined&ist=&jit=&cg=&bdtype=0&oriquery=&objurl=http%3A%2F%2Fimg4.duitang.com%2Fuploads%2Fitem%2F201510%2F08%2F20151008092248_Fjd8S.jpeg&fromurl=ippr_z2C%24qAzdH3FAzdH3Fooo_z%26e3B17tpwg2_z%26e3Bv54AzdH3Frj5rsjAzdH3F4ks52AzdH3F9m0c0dn9aAzdH3F1jpwtsAzdH3F&gsm=0&rpstart=0&rpnum=0', //分享链接
	    imgUrl: 'http://img1.3lian.com/img013/v4/18/d/97.jpg', //分享图标
	    type: 'link', // 分享类型,music、video或link，不填默认为link
	    dataUrl: '', // 如果type是music或video，则要提供数据链接，默认为空
	    success: function () { 
	        // 用户确认分享后执行的回调函数
	    },
	    cancel: function () { 
	        // 用户取消分享后执行的回调函数
	    }
	}); 
	
	//分享到qq
	wx.onMenuShareQQ({
	    title: '测试 分享到qq', // 分享标题
	    desc: '这是一个测试项目,纯属娱乐', // 分享描述
	    link: 'http://image.baidu.com', // 分享链接
	    imgUrl: 'https://ss1.baidu.com/6ONXsjip0QIZ8tyhnq/it/u=1740994930,818057834&fm=58g', // 分享图标
	    success: function () { 
	       // 用户确认分享后执行的回调函数
	    },
	    cancel: function () { 
	       // 用户取消分享后执行的回调函数
	    }
	});
	
	//分享到微博
	wx.onMenuShareWeibo({
	    title: '测试 分享到微博', // 分享标题
	    desc: '这是一个测试项目,纯属娱乐', // 分享描述
	    link: 'http://image.baidu.com', // 分享链接
	    imgUrl: 'http://img1.3lian.com/img013/v4/18/d/97.jpg', // 分享图标
	    success: function () {
	       // 用户确认分享后执行的回调函数
	    },
	    cancel: function () { 
	        // 用户取消分享后执行的回调函数
	    }
	});
	
	//分享到qq空间
	wx.onMenuShareQZone({
	    title: '测试 分享到qq空间', // 分享标题
	    desc: '这是一个测试项目,纯属娱乐', // 分享描述
	    link: 'http://user.qzone.qq.com', // 分享链接
	    imgUrl: 'https://ss1.baidu.com/6ONXsjip0QIZ8tyhnq/it/u=1740994930,818057834&fm=58', // 分享图标
	    success: function () { 
	       // 用户确认分享后执行的回调函数
	    },
	    cancel: function () { 
	        // 用户取消分享后执行的回调函数
	    }
	});
});

function chosePics(){
	wx.chooseImage({
	    count: 2, // 默认9
	    sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
	    sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
	    success: function (res) {
			$("#preview").empty();
	        var localIds = res.localIds; // 返回选定照片的本地ID列表，localId可以作为img标签的src属性显示图片
	        for (var int = 0; int < localIds.length; int++) {
				var img = document.createElement("img");
				img.setAttribute("src", localIds[int]);
				$("#preview").append(img);
			}
	    }
	});
}

function uploadPics(){
	//要上传 ，先的选择照片
	wx.chooseImage({
	    count: 2, // 默认9
	    sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
	    sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
	    success: function (res) {
	        var localIds = res.localIds; // 返回选定照片的本地ID列表，localId可以作为img标签的src属性显示图片
	        //预览选择的照片
	      /*   wx.previewImage({
        	    current: localIds[0], // 当前显示图片的http链接
        	    urls: localIds // 需要预览的图片http链接列表
        	}); */
        	$("#preview").empty();
			syncUpload(localIds);
	    }
	});
	
}
//单张上传，不能多张一起上传
var syncUpload = function(localIds){
	var localId = localIds.pop();alert("进入upload;");
	wx.uploadImage({ 
	    localId: localId, // 需要上传的图片的本地ID，由chooseImage接口获得
	    isShowProgressTips: 1, // 默认为1，显示进度提示
	    success: function (res) {
	        var serverId = res.serverId; // 返回图片的服务器端ID
	      	//显示图片
			var img = document.createElement("img");
			img.setAttribute("src", localId);
			img.onclick = function(){
				downloadImg(serverId);
			}
			$("#preview").append(img);
	        if(localIds.length > 0){
	        	syncUpload(localIds);
	        }
	    }
	});
}

var downloadImg = function(serverId){
	wx.downloadImage({
	    serverId: serverId, // 需要下载的图片的服务器端ID，由uploadImage接口获得
	    isShowProgressTips: 1, // 默认为1，显示进度提示
	    success: function (res) {
	        var localId = res.localId; // 返回图片下载后的本地ID
	    }
	});
}

/* 音频 */
//开始录音接口
function startRecord(){
	//监听语音播放完毕接口  应该放在ready函数里
	wx.onVoicePlayEnd({
	    success: function (res) {
	        var localId = res.localId; // 返回音频的本地ID
	        var text = document.createTextNode("语音播放完毕！");
	        $("#voice").append(text);
	    }
	});
	
	wx.startRecord();
}

//停止录音接口
function stopRecord(){
	wx.stopRecord({
	    success: function (res) {
	        var localId = res.localId;
	        var span = document.createElement("span");
	        var result = "";
	        wx.translateVoice({
     		   localId: localId, // 需要识别的音频的本地Id，由录音相关接口获得
     		    isShowProgressTips: 1, // 默认为1，显示进度提示
     		    success: function (res) {
     		    	result = res.translateResult; // 语音识别的结果
     		        var text = document.createTextNode("识别结果：" + result);
     		        span.appendChild(text);
     		        $("#voice").append(span);
     		    }
     		});
	    }
	});
}
//播放语音接口
function playVoice(localId){
	wx.playVoice({
	    localId: localId // 需要播放的音频的本地ID，由stopRecord接口获得
	});
}
/* 音频 */
 
function getNet(){
	wx.getNetworkType({
	    success: function (res) {
	        var networkType = res.networkType; // 返回网络类型2g，3g，4g，wifi
	        $("#net").append(document.createTextNode("当前网络为：" + networkType));
	    }
	});
}

/* 地理位置 */
function lookLocation(latitude, longitude){
	wx.openLocation({
	    latitude: latitude, // 纬度，浮点数，范围为90 ~ -90
	    longitude: longitude, // 经度，浮点数，范围为180 ~ -180。
	    name: '西安钟楼', // 位置名
	    address: '西安钟楼西安钟楼', // 地址详情说明
	    scale: 28, // 地图缩放级别,整形值,范围从1~28。默认为最大
	    infoUrl: 'https://www.baidu.com' // 在查看位置界面底部显示的超链接,可点击跳转
	});
}
function getLocation(){
	wx.getLocation({
	    type: 'gcj02', // 默认为wgs84的gps坐标，如果要返回直接给openLocation用的火星坐标，可传入'gcj02'
	    success: function (res) {
	        var latitude = res.latitude; // 纬度，浮点数，范围为90 ~ -90
	        var longitude = res.longitude; // 经度，浮点数，范围为180 ~ -180。
	        var speed = res.speed; // 速度，以米/每秒计
	        var accuracy = res.accuracy; // 位置精度
	        
	        lookLocation(latitude, longitude);
	    }
	});
}
/* 地理位置 */
 
/* 界面操作 */
//隐藏右上角菜单
function hideAll(){
	wx.hideOptionMenu();
}
//显示右上角菜单
function showAll(){
	wx.showOptionMenu();
}
function closeInterface(){
	wx.closeWindow();
}
function hideMenuItems(items){
	wx.hideMenuItems({
	    menuList: items // 要隐藏的菜单项，只能隐藏“传播类”和“保护类”按钮，所有menu项见附录3
	});
}
function showMenuItems(items){
	wx.showMenuItems({
	    menuList: items // 要显示的菜单项，所有menu项见附录3
	});
}
function hideAllNonBaseMenuItem(){
	wx.hideAllNonBaseMenuItem();
}
function showAllNonBaseMenuItem(){
	wx.showAllNonBaseMenuItem();
}
/* 界面操作 */

/* 摇一摇周边 */ 	
function startSearchBeacons(){alert("entrance");
	wx.startSearchBeacons({
		ticket:"apple",  //摇周边的业务ticket, 系统自动添加在摇出来的页面链接后面
		complete:function(argv){
			alert(1);
			//开启查找完成后的回调函数
		}
	});
}
/* 摇一摇周边 */ 
</script>
</head>
<style>
img {
    display: block;
    outline: none;
    border:0;
    height: 100%;
    width: 100%;
}
</style>
<body>
<!-- <div style="width:100%;">
	<img src="http://img.zcool.cn/community/01bf1655e514b16ac7251df840273f.jpg"/>
</div> -->
<button onclick="chosePics()">选择照片</button>
<div id="preview">此处预览照片</div><br/><br/>
<button onclick="uploadPics()">上传图片</button>
<br/><br/>
<div id="voice"></div>
<button onclick="startRecord()">开始录音</button>
<button onclick="stopRecord()">停止录音</button>
<button>播放录音</button>
<br/><br/>
<button onclick="getNet()">识别当前网络</button><span id="net"></span>
<br/><br/>
<button onclick="lookLocation(34.223,108.952 )">查看钟楼位置</button><button onclick="getLocation()">获取当前位置</button>
<br/><br/>
<button onclick="hideAll()">隐藏右上角菜单</button>
<button onclick="showAll()">显示右上角菜单</button>
<button onclick="closeInterface()">关闭当前网页窗口接口</button><br/><br/>
<button onclick="hideMenuItems(['menuItem:share:appMessage','menuItem:share:timeline'])">批量隐藏功能按钮接口(共享给朋友、共享到朋友圈)</button>
<button onclick="showMenuItems(['menuItem:share:appMessage','menuItem:share:timeline'])">批量显示功能按钮接口(共享给朋友、共享到朋友圈)</button>
<button onclick="hideAllNonBaseMenuItem()">隐藏所有非基础按钮接口</button>
<button onclick="showAllNonBaseMenuItem()">显示所有功能按钮接口</button>
<hr/>
<button onclick="startSearchBeacons()">开启查找周边ibeacon设备接口</button>
</body>
</html>