<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Insert title here</title>

</head>

<link rel="stylesheet" href="${contextPath }/easyui/css/easyui.css" />
<link rel="stylesheet" href="${contextPath }/easyui/css/icon.css" />
<script type="text/javascript" src="${contextPath }/easyui/js/jquery.min.js"></script>
<script type="text/javascript" src="${contextPath }/easyui/js/jquery.easyui.min.js"></script>


<body>
<div class="easyui-panel" title="New Topic" style="width:400px">
<div style="padding:10px 60px 20px 60px">
<form id="ff" method="post">
	    	<table cellpadding="5">
	    		<tr>
	    			<td>apppid:</td>
	    			<td><input class="easyui-textbox" type="text" name="name" data-options="required:true"></input></td>
	    		</tr>
	    		<tr>
	    			<td>secret:</td>
	    			<td><input class="easyui-textbox" type="text" name="email" data-options="required:true,validType:'email'"></input></td>
	    		</tr>
	    		<tr>
	    			<td>token:</td>
	    			<td><input class="easyui-textbox" type="text" name="subject" data-options="required:true"></input></td>
	    		</tr>
	    		<tr>
	    			<td>accesstoken:</td>
	    			<td></td>
	    		</tr>
	    		<tr>
	    			<td>expiretime:</td>
	    			<td></td>
	    		</tr>
	    	</table>
	    </form>
<div style="text-align:center;padding:5px">
	    	<a href="javascript:void(0)" class="easyui-linkbutton" onclick="submitForm()">Submit</a>
	    	<a href="javascript:void(0)" class="easyui-linkbutton" onclick="clearForm()">Clear</a>
</div>
</div>
</div>
</body>
</html>